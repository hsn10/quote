# Build file for quote
#
# Process this file with http://www.scons.org tool

import os
# init Scons
EnsureSConsVersion(3,0)
EnsurePythonVersion(2,7)

# set defaults
PREFIX='/usr/local'
VERSION='0.10'

env = Environment(LIBPATH=['/usr/lib','/usr/local/lib'], \
                  tools = ['lex','yacc','gcc','gnulink'])

from importer import importEnvironment,importVariable
importEnvironment(env,'HOME')
importVariable(env,'CC')
importVariable(env,'CFLAGS','CCFLAGS')
importEnvironment(env,prefix='DISTCC_')
importEnvironment(env,prefix='CCACHE_')
importEnvironment(env,'PATH')

# Turn CPPFLAGS to list, so we can add values to it
env.Append( CPPFLAGS = [])

# Get CC from commandline
if ARGUMENTS.get('CC', 0):
    env.Replace(CC =  ARGUMENTS.get('CC'))

if ARGUMENTS.get('CFLAGS',0):
   env.Replace(CCFLAGS = ARGUMENTS.get('CFLAGS'))
if ARGUMENTS.get('CCFLAGS',0):
   env.Replace(CCFLAGS = ARGUMENTS.get('CCFLAGS'))

# Convert CCFLAGS into list
if not 'CCFLAGS' in env:
   env.Replace(CCFLAGS = '')
env.Replace(CCFLAGS = str(env['CCFLAGS']).split(' '))

############  Start configuration   ##############

from compilertest import checkForCCOption
from prefix import checkForUserPrefix
from debugmode import checkForDebugBuild

conf = Configure(env,{'checkForCCOption':checkForCCOption,
                      'checkPrefix':checkForUserPrefix,
                      'checkForDebugBuild':checkForDebugBuild
                      })
if not conf.CheckCC(): Exit(1)
# check for CC options
for option in Split("""
      -Wall -W -Wstrict-prototypes -Wmissing-prototypes -Wshadow
      -Wbad-function-cast -Wcast-qual -Wcast-align -Wwrite-strings
      -Waggregate-return -Wmissing-declarations
      -Wmissing-format-attribute -Wnested-externs
      -ggdb -fno-common -Wchar-subscripts -Wcomment
      -Wimplicit -Wsequence-point -Wreturn-type
      -Wfloat-equal -Wno-system-headers -Wredundant-decls
      -pedantic
      -Wlong-long -Wundef -Winline
      -Wpointer-arith -Wno-unused-parameter
      -Wunreachable-code
      -fmacro-backtrace-limit=2 -Wno-cast-align -Wno-pointer-sign
"""):
       conf.checkForCCOption(option)

# Portability build time config
if conf.CheckFunc('srandomdev'):
    conf.env.Append(CPPFLAGS = '-DHAVE_SRANDOMDEV')
if conf.CheckFunc('random'):
    conf.env.Append(CPPFLAGS = '-DHAVE_RANDOM')
if conf.CheckCHeader('malloc.h'):
    conf.env.Append(CPPFLAGS = '-DHAVE_MALLOC_H')
if conf.CheckCHeader('unistd.h'):
    conf.env.Append(CPPFLAGS = '-DHAVE_UNISTD_H')
if conf.CheckCHeader('limits.h'):
    conf.env.Append(CPPFLAGS = '-DHAVE_LIMITS_H')
PREFIX=conf.checkPrefix(PREFIX)
conf.CheckLib('ncurses')
conf.checkForDebugBuild()
conf.Finish()

env.Append(CPPFLAGS = "-DPACKAGE_VERSION=\\\""+VERSION+"\\\"")

Export( Split("env PREFIX") )

# process build rules
env.SConscript(dirs=Split(". src"))
