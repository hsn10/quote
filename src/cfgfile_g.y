%{
#include <string.h>
#include <stdlib.h>
#include "quote.h"
void yyerror (char *s)
{
         fprintf (stderr, "%s\n", s);
}
extern int yylex(void);
#define YYERROR_VERBOSE 1
#define YYDEBUG 1
%}
%union {
	int intval;
	double floatval;
	char * string;
	}
%type <string> restofline groupline grouphead
%token <intval> INTEGER 
%token <floatval> FLOAT
%token <string> EOL
%token <string> COMMENT
%token <string> OPTIONS
%token <string> STRING
%token <string> GROUP_START
%token <string> GROUP_END
%token <string> EVENT_START
%token <string> EVENT_END
%token <string> GROUP_MEMBER
%token <string> EVENT_MEMBER

%%
input: /*empty */
	| input EOL
	| input grouphead EOL groupline GROUP_END
	| input eventhead EOL eventline EVENT_END
	| input COMMENT
;
restofline: /*empty*/ { $$="";}
	| restofline STRING 
	| restofline FLOAT  { $$="";}
	| restofline INTEGER  { $$="";}
;
grouphead: GROUP_START STRING FLOAT
	|  GROUP_START STRING
	|  GROUP_START STRING INTEGER
;	
eventhead: EVENT_START STRING {;}
;	
groupline:   groupline GROUP_MEMBER STRING EOL
	   | groupline GROUP_MEMBER STRING FLOAT EOL
	   | groupline GROUP_MEMBER STRING INTEGER EOL
	   | groupline OPTIONS restofline EOL {;} 
	   | groupline COMMENT EOL
	   | groupline EOL
	   | /* empty */ {;}
;
eventline: eventline EVENT_MEMBER restofline EOL
	|  eventline EOL
	|  eventline COMMENT EOL
	|  /* empty */ {;}
;	
%%
// #include "cfgfile.c"
