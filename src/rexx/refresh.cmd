/* Refresh 0.12 */

/* Pouziti: refresh.cmd [index] */

/* Obnovi soubor index, ktery obsahuje
jmeno soboru s citaty  pocet citatu <CR/LF> */
/* odkazy na neexistujici soubory smaze */
/* prvni prazdna radka prerusi zpracovani index souboru */

/* errorlevels:
   0 - OK
   1 - Index file cannot be opened
   2 - Chyba pri zakladani prac. souboru */

/* argument - jmeno index. souboru */

/* load the neccessary functions from REXXUTIL */
call rxFuncAdd 'SysTempFileName', 'REXXUTIL', 'SysTempFileName'
call rxFuncAdd 'SysFileDelete', 'REXXUTIL', 'SysFileDelete'


parse arg idx
if length(idx)=0 then idx='index'
if stream(idx,'C','OPEN READ')\='READY:' then return 1;
prac=SysTempFileName('REFRESH.???');
if stream(prac,'C','OPEN WRITE')\='READY:' then return 2;

say 'Refreshing quote index file 'idx

radka=linein(idx);
/* hlavni smycka */
do while length(radka)>0
  parse var radka jmeno pocet
/*if pocet='' then pocet=0*/
  pocet=spoctiradky(jmeno);
  if pocet\=-1 then call lineout prac,jmeno pocet
/* a znova */
  radka=linein(idx);
end
/* uzavreme soubory */
call stream idx,'C','CLOSE'
call stream prac,'C','CLOSE'
/* a prejmenujeme prac na idx */
'@copy 'prac idx '>NUL'
'@del 'prac '/f'
say 'Refresh was succesfull.'
return 0

/* Spocita radky v souboru */
spoctiradky: procedure
parse arg jm
  if stream(jm,'C','OPEN READ')\='READY:' then return -1;
  say 'Counting records and indexing 'jm
  ix=indexjm(jm);
  sp=SysFileDelete(ix);
  if (sp>2) then ix='NUL' /* radsi s nim nebudu nic delat */
  if stream(ix,'C','OPEN WRITE')\='READY:' then ix='NUL';
  n=stream(jm,'c','SEEK <0'); /* zname konec ! */
  if n<3 then
    do
     call stream jm,'C','CLOSE'
     call stream ix,'C','CLOSE'
     say ' ..  empty file.'
     return 0
    end
  call stream jm,'c','SEEK 1'
  sp=1
  linek=0;
  do while sp<n
    linka=strip(linein(jm));
    if left(linka,1)=';' then do
    			       say "  "right(linka,length(linka)-1)
    			       linka=''
                              end
    if left(linka,1)\='"' then linka=''
    if length(linka)>0 then do
                              linek=linek+1;
                              call lineout ix,left(sp,7);
                            end
    sp=stream(jm,'c','SEEK');
  end
  call stream jm,'C','CLOSE'
  call stream ix,'C','CLOSE'
  say ' .. ' linek 'records found.'
return linek

/* vrati jmeno indexoveho souboru */
/* inteligentne doplni na konec X */
/* karel.txt -> karel.X */
/* lopata -> lopata.X */

indexjm: procedure
parse arg jmeno
  i=1;
  lp=0;
  /* osetrim zacatek na tecku */
  if POS('.',jmeno)=1 then return jmeno'X'
  do while i\=0
    lp=i;
    i=POS('.',jmeno,lp+1);
  end
  if lp=1 then return jmeno'.X'
return substr(jmeno,1,lp)'X'
