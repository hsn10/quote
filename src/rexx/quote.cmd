/* Quote REXX Engine */
/* verze 0.09 */
/* argument - jmeno index souboru */

/* errorlevely: */
/* 1 - index soubor je necitelny..*/
/* 2 - nenalezeny zadne citaty */

/* Co mam delat:*/
/* 1. precist z indexu pocet citatu */
/* 2. Vygenerovat nah. cislo 1-pocet citatu */
/* 3. Najit v jakem je pozadovany citat souboru */
/* 4. Potom ho precist */
/* 5. Zformatovat ho a vytisknout */

call RxFuncAdd 'SysTextScreenSize', 'RexxUtil', 'SysTextScreenSize'

parse arg idx
if length(idx)=0 then idx='index'
if stream(idx,'C','OPEN READ')\='READY:' then return 1;
/* krok 1 */
citatu=spocitejcitaty(idx);
if citatu=0 then return 2;
say citatu 'citatu nalezeno.'

/* kroky 2,3,4 */

citat=precticitat(idx,random(1,citatu));
/* citat=precticitat(idx,2); */ /* TIME TEST ONLY ! */

call tisknicitat(citat)
return 0;

tisknicitat: procedure
parse arg zdroj
parse value zdroj with """" citat """,""" dopl """,""" kdo """,""" kde """"

say "1B"x || "[1;33m"
call vystup citat,0
call charout stdout, "1B"x || "[0;36m"
call vystup dopl,0
call charout stdout, "1B"x || "[0;32m"
call vystup kdo,2
call charout stdout, "1B"x || "[0m"
call vystup kde,2
say "1B"x || "[0m" /* default */
return

spocitejcitaty: procedure
parse arg idx
  n=0;
  do forever
    radka=linein(idx)
    if length(radka)=0 then return n;
    parse var radka junk pocet
    if length(pocet)>0 then n=n+pocet
  end
return

precticitat: procedure
parse arg idx,n
  call stream idx,'C','SEEK 1'
  nasli=0;
  do while nasli=0
    radka=linein(idx);
    parse var radka soubor cislo
    if n>cislo then
                 do
                   n=n-cislo
                   iterate;
                 end
    nasli=1;
  end
  call stream idx,'C','CLOSE'
  if stream(soubor,'C','OPEN READ')\='READY:' then return;
  idx=indexjm(soubor);

/*  say 'n='n */

  if (stream(idx,'C','OPEN READ')\='READY:') then
  do /* hledej klasicky */
    do nasli=1 to n
      radka=linein(soubor);
    end
  end
  else
  do
/*  say 'index search!'*/
    call stream idx,'C','SEEK '1+(n-1)*9
    nasli=linein(idx);
    call stream soubor,'C','SEEK 'nasli
    radka=linein(soubor);
  end
  call stream idx,'C','CLOSE'  
  call stream soubor,'C','CLOSE'
return radka

/* vytiskne retezec na obrazovku s formatovanim */
/* zarovnani 0/1 - nedelit/delit slova (Word wrap) */
/* 0/2 - zarovnavat na levy,pravy okraj */

formatvystup: procedure
parse arg retezec,zarov
  if length(retezec)=0 then return
  /* setup - nastaveni veci s bitem 0 */
  wwrap=1;
  just=1;
  /* a doladeni podle zarov. */
  if zarov>1 then
    do
      zarov=zarov-2;
      just=2; /* just=1 levy okraj, just=2 pravy */
    end
  if zarov>0 then
    do
      zarov=zarov-1;
      wwrap=0;
    end
  parse value SysTextScreenSize() with row col
  retezec=SPACE(retezec,1);
  rpos=1;
  rvel=length(retezec);
  do while rpos<rvel
    /* nejdrive ale preskoc mezery */
    do while (substr(retezec,rpos,1)=' ')&(rpos<rvel)
      rpos=rpos+1;
    end
    if wwrap=0 then do
                      tisk=substr(retezec,rpos,MIN(col,rvel-rpos+1))
                      rpos=rpos+MIN(col,rvel-rpos+1);
                    end
               else do
                    skonci=0
                    zbyva=col
                    tisk='';
                    do while skonci=0
                       i=POS(' ',retezec,rpos+1);
                       /* say 'i='i */
                       if (i=0) & (rvel-rpos+1<zbyva) then do
                         tisk=tisk''substr(retezec,rpos,MIN(zbyva,rvel-rpos+1))
                         rpos=rpos+MIN(col,rvel-rpos+1);
                         skonci=1;
                         iterate;
                       end
                       if i=0 then do
                           skonci=1
                           iterate
                       end
                       if i-rpos<=zbyva then do
                          tisk=tisk''substr(retezec,rpos,i-rpos+1);
                          zbyva=zbyva-(i-rpos+1);
                          rpos=rpos+(i-rpos+1);
                          end
                          else
                          do
                           skonci=1;
                          end
                    end
               end
  if just=1 then tisk=left(tisk,col);
  if just=2 then tisk=left(' ',col-length(tisk))''tisk
  call charout stdout,tisk
  end
return

/* dela konverze \n na novy odstavec (3x 0d255 */
/* /r na CRLF */

vystup: procedure
parse arg retezec,zarov
  i=pos('\',retezec);
  if i=0 then do
    call formatvystup retezec,zarov
    return
    end
  zac=1;
  do while i\=0
    if i=length(retezec) then do i=0;iterate;end
    if substr(retezec,i+1,1)='n' then
    do
      ret=substr(retezec,zac,i-zac);
      call formatvystup ret,zarov
      zac=i+2;
      if zac>=length(retezec) then return
      retezec='���'right(retezec,length(retezec)-zac+1);
      zac=1;
      i=POS('\',retezec,zac);
      iterate;
    end
    if substr(retezec,i+1,1)='r' then
    do
      ret=substr(retezec,zac,i-zac);
      call formatvystup ret,zarov
      zac=i+2;
      if zac>=length(retezec) then return
      i=POS('\',retezec,zac);
      iterate;
    end
    i=POS('\',retezec,i+1);
  end
  ret=right(retezec,length(retezec)-zac+1)
  call formatvystup ret,zarov
return

/* vrati jmeno indexoveho souboru */
/* inteligentne doplni na konex X */
/* karel.txt -> karel.X */
/* lopata -> lopata.X */

indexjm: procedure
parse arg jmeno
  i=1;
  lp=0;
  /* osetrim zacatek na tecku */
  if POS('.',jmeno)=1 then return jmeno'X'
  do while i\=0
    lp=i;
    i=POS('.',jmeno,lp+1);
  end
  if lp=1 then return jmeno'.X'
return substr(jmeno,1,lp)'X'
