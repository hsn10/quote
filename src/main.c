#include <stdio.h>
#include <stdlib.h>
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#include <time.h>
#include "quote.h"

/* gettext switch */
#ifdef USE_GETTEXT
#include <locale.h>
#include <libintl.h>
#define _(String) gettext (String)
#else
#define _(String) (String)
#endif

int
main (int argv, char *argc[])
{
  struct quotes *q;
  struct quote *qt;
  struct prepared_quote *pq;
  struct group_list *gl;
  
  char *fname;
  int pos;

#ifdef USE_GETTEXT  
  setlocale(LC_MESSAGES,"");
  bindtextdomain(PACKAGE,LOCALEDIR);
  textdomain(PACKAGE);
#endif
  readconfig("quote.cfg",&gl);

  // refresh_index ("index");
  q = read_index ("index", NULL);
  //q=read_index_file("bam.txt",q);
  //print_quotes_index(q);
  init_random();
  if (random_quote (q, &fname, &pos))
  {
    fprintf (stderr, _("Random quote failed!\n"));
    return 1;
  }
  printf ("Random quote selected. file=%s number=%d\n", fname, pos);
  qt = read_quote (fname, pos);
  ui_init ();

  //printf ("Quote:%s\n%s\n%s\n%s\n", qt->part[0], qt->part[1], qt->part[2],
  //	  qt->part[3]);
  pq=ui_preformat_quote (qt);
  ui_display_quote(pq);
  free_prepared_quote(pq);
  free_quote (qt);
  free_quotes_index (q);
  ui_terminate ();
  return 0;
}
