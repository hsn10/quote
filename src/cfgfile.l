%option caseless
%{
#include "cfgfile_g.h"
%}
%pointer
DIGIT [0-9]
INTEGER {DIGIT}+
FLOAT {INTEGER}*\.{INTEGER}+
KEYWORD [a-zA-Z]+
WHITESPACE [ \t]+
EOL \r?\n
COMMENT #.*
STRING [^ \t\r\n]+
%%
{WHITESPACE}	; /* ignore */
{EOL}		yylval.string="";return EOL;
{FLOAT}		yylval.floatval=atof(yytext);return FLOAT;
{INTEGER}	yylval.intval=atoi(yytext);return INTEGER;
{COMMENT}      	yylval.string=yytext;return COMMENT; 
"group"		yylval.string=yytext;return GROUP_START;
"endgroup"	yylval.string=yytext;return GROUP_END;
"index"|"file"|"directory"|"fortune" yylval.string=yytext;return GROUP_MEMBER;
"options"	yylval.string=yytext;return OPTIONS;
"event"		yylval.string=yytext;return EVENT_START;
"endevent"	yylval.string=yytext;return EVENT_END;
"enable"|"disable"|"priority" yylval.string=yytext;return EVENT_MEMBER;
{STRING}	yylval.string=yytext;return STRING;
%%
int yywrap(void) { return 1; }
