#include <stdio.h>
#ifdef HAVE_MALLOC_H
#include <malloc.h>
#else
#include <stdlib.h>
#endif
#include "quote.h"

/* FLEX + BISON */
extern FILE *yyin, *yyout;
int yyparse (void);

int readconfig(const char *file,struct group_list **gl)
{
	FILE *f;
	/*
	int number;
	char *line;
	char *token;
	char *token2;
	char *token3;
	int i;
	*/
	f=fopen(file,"r");
	if(f==NULL) return -1; /* file NOT Found */
	/*
	line=token=token2=token3=NULL;
	
	number=0;
	while(1)
	{
		if(line!=NULL) free(line);
		if(token!=NULL) free(token);
		if(token2!=NULL) free(token2);
		if(token3!=NULL) free(token3);

	        line=token=token2=token3=NULL;

		i=fscanf(f,"%a[^\r\n]",&line);
		if(feof(f)) break;
		number++;
		skipEOL(f);
		if(i==0) continue;
		if(strlen(line)==0) continue;
		if(line[0]=='#' || line[0]==';') continue;
		i=sscanf(line,"%as%as%as",&token,&token2,&token3);
		printf("%d: %s\n T=%s T2=%s T3=%s\n",number,line,token,token2,token3);
	}
	*/
	yyin=f;
	yyparse();
	fclose(f);
	return 0;
}
