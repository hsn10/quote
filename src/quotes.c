#include <stdlib.h>
#include <string.h>
#include "quote.h"

void
print_quotes_index (const struct quotes *q)
{
  const struct quotes *qtmp;
  qtmp = q;
  while (qtmp != NULL)
    {
      printf ("File: %s - %d quotes\n", qtmp->filename, qtmp->quotes);
      qtmp = qtmp->next;
    }
}

void
free_quotes_index (struct quotes *q)
{
  struct quotes *qtmp;
  qtmp = q;
  while (qtmp != NULL)
    {
      free (qtmp->filename);
      q = qtmp;
      qtmp = qtmp->next;
      free (q);
    }
}

void
free_prepared_quote (struct prepared_quote *q)
{
  int i;
  if (q == NULL)
    return;
  for (i = 0; i < QUOTE_FIELDS; i++)
    if (q->part[i] != NULL)
    	free (q->part[i]);
  free (q);
}

void
free_quote (struct quote *q)
{
  int i;
  if (q == NULL)
    return;
  for (i = 0; i < QUOTE_FIELDS; i++)
    if (q->part[i] != NULL)
      free (q->part[i]);
  free (q);
}

/* delka citatu v bajtech, priblizne */
int
quote_size (struct quote *q)
{
  int res = 0;
  int i;
  if (q == NULL)
    return 0;
  for (i = 0; i < QUOTE_FIELDS; i++)
    if (q->part[i] != NULL)
      res += strlen (q->part[i]);
  return res;
}

int
count_quotes (const struct quotes *q)
{
  int count = 0;
  const struct quotes *qtmp;
  if (q == NULL)
    return 0;
  qtmp = q;
  while (qtmp != NULL)
    {
      count += qtmp->quotes;
      qtmp = qtmp->next;
    }
  return count;
}

float
count_w_quotes (const struct quotes *q)
{
  float count = 0;
  const struct quotes *qtmp;
  if (q == NULL)
    return 0;
  qtmp = q;
  while (qtmp != NULL)
    {
      count += qtmp->w * qtmp->quotes;
      qtmp = qtmp->next;
    }
  return count;
}

struct quotes *
append_quote (struct quotes *q, struct quotes *qtmp)
{
  struct quotes *qtmp2;
  /* pripojit qtmp k q *
   * TODO: Dupe checking??
   *                     */
  if (qtmp == NULL)
    return q;
  if (qtmp->filename == NULL ||
      qtmp->quotes == 0)
    return q;
  qtmp->next = NULL;
  if (qtmp->w <= 0.0f)
    qtmp->w = 1.0f;
  if (q == NULL)
    q = qtmp;
  else
    {
      qtmp2 = q;
      while (qtmp2->next != NULL)
	qtmp2 = qtmp2->next;
      qtmp2->next = qtmp;
    }
  return q;
}

/* vybere nahodny citat z quote groupy */
int
random_quote (const struct quotes *q, char **fname, int *pos)
{
  float count = count_w_quotes (q);
  const struct quotes *qtmp;
  float j;

  if (count == 0)
    return -1;
  j = (double) (count) * rand () / (RAND_MAX + 1.0f);
  /* printf ("random=%f max=%f\n", j, count); */

  qtmp = q;
  while (qtmp != NULL)
    {
      if (qtmp->quotes * qtmp->w >= j)
	{
	  *fname = qtmp->filename;
	  *pos = 1 + j / qtmp->w;
	  return 0;
	}
      j -= qtmp->quotes * qtmp->w;
      qtmp = qtmp->next;
    }

  return -1;
}

/* nacte citat ze souboru do pameti */
struct quote *
read_quote (const char *fname, int pos)
{
  struct quote *q;
  int i;
  int state, delim, c, escape;
  FILE *f;

  q = malloc (sizeof (struct quote));
  if (q == NULL)
    return NULL;
  for (i = 0; i < QUOTE_FIELDS; i++)
    q->part[i] = NULL;
  f = fopen (fname, "r");
  if (f == NULL)
    {
      free (q);
      return NULL;
    }
  i = quote_offset (fname, pos);
  if (i < 0)
    {
      free (q);
      return NULL;
    }
  if (0 != fseek (f, i, SEEK_SET))
    {
      free (q);
      return NULL;
    }
  state = 0;
  delim = 0;
  escape = 0;

  while (1)
    {
      skipWhitespace (f);
      if (feof (f))
	break;
      c = getc (f);
      if (c == '\r' || c == '\n')
	{
	  ungetc (c, f);
	  skipEOL (f);
	  continue;
	}
      if (c == '"')
	{
	  if (state > 0)
	    break;
	  state = 1;
	  delim = 0;
	  escape = 0;
	}
      else
	ungetc (c, f);
      while (1)
	{
	  c = getc (f);
	  if (c == EOF)
	    break;
	  if (c == '\r' || c == '\n')
	    {
	      ungetc (c, f);
	      skipEOL (f);
	      add_char_to_string (&q->part[state - 1], ' ');
	      break;
	    }
	  /* pridat nacteny znak do citatu */
	  if (delim == 0 && state > 0)
	    {
	      if (escape == 1)
		add_char_to_string (&q->part[state - 1], c);
	      else if (c != '"')
		add_char_to_string (&q->part[state - 1], c);
	    }
	  if (c == '\\')
	    {
	      if (escape == 0)
		escape = 1;
	      else
		escape = 0;
	      continue;
	    }
	  if (escape == 1)
	    {
	      escape = 0;
	      continue;
	    }
	  if (c == '"')
	    {
	      if (delim == 1)
		{
		  delim = 0;
		  continue;
		}
	      else
		{
		  if (state == QUOTE_FIELDS)
		    {
		      fclose (f);
		      return q;
		    }
		  delim = 1;
		  skipWhitespace (f);
		  continue;
		}
	    }
	  if (delim == 1)
	    {
	      if (c == ',')
		state++;
	      if (state > QUOTE_FIELDS)
		{
		  fclose (f);
		  return q;
		}
	      skipWhitespace (f);
	      continue;
	    }
	}

    }
  fclose (f);
  return q;
}

char **
add_char_to_string (char **str, char c)
{
  if (*str == NULL)
    {
      if (NULL == (*str = calloc (1, 2)))
	return NULL;
    }
  else
    {
      *str = realloc (*str, strlen (*str) + 2);
      if (*str == NULL)
	return NULL;

    }
  (*str)[strlen (*str) + 1] = '\0';
  (*str)[strlen (*str) + 0] = c;
  return str;
}
