#ifndef QUOTE_H
#define QUOTE_H 1

#define JUSTIFY_LEFT 0
#define JUSTIFY_RIGHT 1
#define JUSTIFY_CENTER 2
#define JUSTIFY_MAX JUSTIFY_CENTER
#define JUSTIFY_MIN JUSTIFY_LEFT

#define QUOTE_FIELDS	4
#define INDEX_EXT	".X"
#define INDEX_ELEMENT	9
#define MAX_ERRORS	20

#define FILETYPE_QUOTE 0
#define FILETYPE_FORTUNE 1
#define FILETYPE_MIN FILETYPE_QUOTE
#define FILETYPE_MAX FILETYPE_FORTUNE

struct quotes
  {
    char *filename;
    int filetype;
    float w;
    int quotes;
    struct quotes *next;
  };

struct quotes_group
{
  char *groupname;
  float w;
  int count;
  struct quotes *quotes;
};

struct group_list
{
	struct quotes_group *group;
	struct group_list *next;
};

struct quote
  {
    char *part[QUOTE_FIELDS];
  };
  
struct prepared_quote
{
    void *part[QUOTE_FIELDS];
    int  line;
};

/* from index.c */
int index_file (const char *filename);
char *indexjm (char *basename, const char *ext);
int refresh_index (const char *filename);
struct quotes *read_index (const char *filename, struct quotes *q);
struct quotes *read_index_file (const char *filename, struct quotes *q);
int quote_offset (const char *filename, int pos);

/* from util.c */
#include <stdio.h>
void skipWhitespace (FILE * f);
void skipLine (FILE * f);
void skipEOL (FILE * f);
int nextcharfromstring(char **str);
char* wordfromstring(char **str);
char **add_word_to_line(char **line,const char *word,unsigned int maxsize);
char** justify_line(char **line,int direction, unsigned int size, int wrap);
void init_random(void);
char **append_string(char **line,const char *word);

/* from quotes.c */
void print_quotes_index (const struct quotes *q);
struct quotes *append_quote (struct quotes *qtmp, struct quotes *q);
int random_quote (const struct quotes *q, char **fname, int *pos);
void free_quotes_index (struct quotes *q);
struct quote *read_quote (const char *fname, int pos);
void free_quote (struct quote *q);
extern /*@alt void@*/ char **  add_char_to_string (char **, char);
float count_w_quotes (const struct quotes *q);
int quote_size (struct quote *q);
void free_prepared_quote (struct prepared_quote *q);

/* from UI */
void ui_init(void);
void ui_run(void);
void ui_terminate(void);
struct prepared_quote *ui_preformat_quote(struct quote *q);
void ui_display_quote(struct prepared_quote *q);

/* from config.c */
int readconfig(const char *file,struct group_list **gl);

#ifdef DMALLOC
#define DMALLOC_FUNC_CHECK 1
#include <dmalloc.h>
#endif
#endif
