#include <curses.h>
#include <term.h>
#include <stdlib.h>
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#ifdef USE_GETTEXT
#include <libintl.h>
#define _(String) gettext (String)
#else
#define _(String) (String)
#endif
#include "quote.h"

int termwidth = 80;
int termheight = 25;
int autowrap = 0;		/* autowrap at last col.? */
int colors=0;

void 
ui_init ()
{
  int err;
  if(isatty(STDOUT_FILENO))
  {
  use_env(TRUE);
  if(OK!=setupterm(NULL,STDOUT_FILENO,&err))
  {
#ifdef DEBUG
	  fprintf(stderr,_("Setup term failed, error=%d\n"),err);
#endif
  }
  termheight=tigetnum("lines");
  if(termheight<0)
  {
#ifdef DEBUG
	  fprintf(stderr,_("Can't get number of screen lines. Err=%d\n"),tigetnum("lines"));
#endif
	  termheight=25;
  }
  termwidth=tigetnum("cols");
  if(termwidth<0)
  {
#ifdef DEBUG
	  fprintf(stderr,_("Can't get number of screen columns. Err=%d\n"),tigetnum("cols"));
#endif
	  termwidth=80;
  }
  autowrap=tigetflag("am");
  if(autowrap<0)
  {
#ifdef DEBUG
	  fprintf(stderr,_("Can't get settings for automargin. Err=%d\n"),tigetflag("am"));
#endif
	  autowrap=0;
  }
  colors=tigetnum("colors");
  if(colors<0)
  {
#ifdef DEBUG
	  fprintf(stderr,_("Can't get number of colors. Err=%d\n"),tigetnum("colors"));
#endif
          colors=0;
  }
  }			  
}
void 
ui_run ()
{

}

void 
ui_terminate ()
{
  //reset_shell_mode();
  del_curterm(cur_term);
}

void handle_line(char **line,int i,struct prepared_quote *prep)
{
	
	      if(i<2)
		      justify_line (line, JUSTIFY_LEFT, termwidth, autowrap);
	      else
		      justify_line (line, JUSTIFY_RIGHT, termwidth, autowrap);
	      append_string((char **)&prep->part[i],*line);
	      if(autowrap==0)
		      append_string((char **)&(prep->part[i]),"\n");
	      prep->line++;
	      free (*line);
}
		
struct prepared_quote *
ui_preformat_quote (struct quote *q)
{
  int i;
  char *str;
  char *word, *line;
  struct prepared_quote *prep;
  if (q == NULL)
    return NULL;
  prep = malloc (sizeof (struct prepared_quote));
  if (prep == NULL)
    return NULL;
  prep->line = 0;
  for (i = 0; i < QUOTE_FIELDS; i++)
    {
      prep->part[i]=NULL;
      str = q->part[i];
      if (str == NULL)
	continue;
      line = NULL;
      while (1)
	{
	  word = wordfromstring (&str);
	  if (word == NULL)
	    {
		    handle_line(&line,i,prep);
	      break;
	    }
	  if (NULL == add_word_to_line (&line, word, termwidth))
	    {
	      handle_line(&line,i,prep);
	      line = NULL;
	      add_word_to_line (&line, word, termwidth);
	    }
	  free (word);
	}
    }
  return prep;
}

void ui_display_quote(struct prepared_quote *q)
{
	int i;
	
	if(q==NULL) return;
        if(colors>0)
	{
		printf("%s\n",tigetstr("bold")
				);
	}
	for(i=0;i<QUOTE_FIELDS;i++)
		if(q->part[i]!=NULL) printf("%s",(char *)q->part[i]);
	printf("\n");
}
