#include <stdio.h>
#include <string.h>
#ifdef HAVE_LIMITS_H
#include <limits.h>
#endif
#ifdef HAVE_MALLOC_H
#include <malloc.h>
#else
#include <stdlib.h>
#endif
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#include <sys/types.h>
#include <sys/stat.h>
#ifdef USE_GETTEXT
#include <libintl.h>
#define _(String) gettext (String)
#else
#define _(String) (String)
#endif
#include "quote.h"

/* vrati jmeno indexoveho souboru */
/* inteligentne doplni na konex X */
/* karel.txt -> karel.X */
/* lopata -> lopata.X */
/* .lamer -> .lamerX  */
/* .lamer.doc -> .lamer.X */

char *
indexjm (char *basename, const char *ext)
{
  char *dot;

  dot = rindex (basename, '.');
  if (dot == NULL)
    return strcat (basename, ext);
  if (dot == basename)
    return strcat (basename, ext + 1);
  *dot = '\0';

  return strcat (basename, ext);
}

int
quote_offset (const char *filename, int pos)
{
  FILE *f;
  char *tmpfn;
  int off;

  tmpfn = malloc (FILENAME_MAX + 1);
  if (tmpfn == NULL)
    {
      return -3;
    }
  strcpy (tmpfn, filename);
  indexjm (tmpfn, INDEX_EXT);
  f = fopen (tmpfn, "r");
  free (tmpfn);
  if (f == NULL)
    return -1;
  if (0 != fseek (f, (pos - 1) * INDEX_ELEMENT, SEEK_SET))
    {
      fclose (f);
      return -2;
    }
  if (1 != fscanf (f, "%d", &off))
    {
      fclose (f);
      return -2;
    }
  fclose (f);
  return off;
}

int
refresh_index (const char *filename)
{
  FILE *f;
  FILE *i;
  char *tn, *tmpfn;
  int c;

  tmpfn = malloc (FILENAME_MAX + 1);
  if (tmpfn == NULL)
    {
      return -3;
    }
  tn = malloc (FILENAME_MAX + 1);
  if (tn == NULL)
    return -3;
  strcpy (tn, filename);
  tn = indexjm (tn, ".tmp");
  f = fopen (filename, "r");
  if (f == NULL)
    {
      free (tn);
      return -1;
    }
  i = fopen (tn, "w");
  if (i == NULL)
    {
      fclose (f);
      free (tn);
      return -2;
    }
  /* try to set tmpfilemode to -w------- */
  chmod (tn, S_IWUSR);

  for (;;)
    {
      if (EOF == fscanf (f, "%s%*[\n\r 0-9]", tmpfn))
	break;
      /* fprintf(stderr,"Read %s\n",tmpfn); */
      fprintf (i, "%s ", tmpfn);
      c = index_file (tmpfn);
      if (c < 0)
	fprintf (i, "\n");
      else
	fprintf (i, "%d\n", c);

    }

  fclose (i);
  fclose (f);
  free (tmpfn);
  /* rename tn to orig */
  if (0 != rename (tn, filename))
    {
      fprintf (stderr, _("Rename %s to %s failed.\n"), tn, filename);
      unlink (tn);
      free (tn);
      return -2;
    }
  chmod (filename, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
  free (tn);
  return 0;
}
/* prida soubor pokud obsahuje citelny index.X do
 * seznamu souboru citatu
 */
struct quotes *
read_index_file (const char *filename, struct quotes *q)
{
  char *tmpfn;
  struct stat buf;
  struct quotes *qtmp;
  tmpfn = malloc (FILENAME_MAX + 1);
  if (tmpfn == NULL)
    return q;
  strcpy (tmpfn, filename);
  indexjm (tmpfn, INDEX_EXT);
  for (;;)
    {

      if (0 != access (tmpfn, R_OK))
	break;
      if (0 != stat (tmpfn, &buf))
	break;
      qtmp = malloc (sizeof (struct quotes));
      if (qtmp == NULL)
	break;
      qtmp->filename = malloc (strlen (filename) + 1);
      if (qtmp->filename == NULL)
	{
	  free (qtmp);
	  break;
	}
      free (tmpfn);
      strcpy (qtmp->filename, filename);
      qtmp->quotes = buf.st_size / INDEX_ELEMENT;
      qtmp->next = NULL;
      return append_quote (q, qtmp);
    }
  free (tmpfn);
  return q;
}
struct quotes *
read_index (const char *filename, struct quotes *q)
{
  FILE *f;
  char *tmpfn;
  struct quotes *qtmp;
  char *stmp;
  int quotes;
  tmpfn = malloc (FILENAME_MAX + 1);
  if (tmpfn == NULL)
    return q;
  f = fopen (filename, "r");
  if (f == NULL)
    {
      free (tmpfn);
      return q;
    }

  for (;;)
    {
      if (EOF == fscanf (f, "%s", tmpfn))
	break;
      if (fscanf (f, "%d\n", &quotes) < 1)
	continue;
      if (quotes <= 0)
	continue;
      qtmp = malloc (sizeof (struct quotes));
      if (qtmp == NULL)
	break;
      stmp = malloc (strlen (tmpfn) + 1);
      if (stmp == NULL)
	break;
      strcpy (stmp, tmpfn);
      qtmp->filename = stmp;
      qtmp->quotes = quotes;
      qtmp->next = NULL;
      /* pripojit qtmp k q *
       * TODO: Dupe checking??
       */
      q = append_quote (q, qtmp);

    }
  free (tmpfn);
  fclose (f);
  return q;
}

/* returns number of quotes indexed or -1 if there was a FATAL
   error, such as file not found */

int
index_file (const char *filename)
{
  FILE *f;
  FILE *i;
  char *tmpfn;
  int state, delim;
  int c, line, escape;
  int count;
  int errors=0;

  f = fopen (filename, "rb");
  if (f == NULL)
    return -1;
  tmpfn = malloc (FILENAME_MAX + 1);
  if (tmpfn == NULL)
    {
      fclose (f);
      return -1;
    }
  strcpy (tmpfn, filename);
  i = fopen (indexjm (tmpfn, INDEX_EXT), "w");
  if (i == NULL)
    {
      fclose (f);
      free (tmpfn);
      return -1;
    }
  state = 0;			/* initial whitespace on line */
  line = 0;
  delim = 0;
  count = 0;
  for (;;)
    {
	    if(errors>=MAX_ERRORS) {
		    fprintf(stderr,"%s:%d Too many errors (%d)\n",filename,line,errors);
		    break;
	    }
      line++;
      skipWhitespace (f);
      escape = 0;
      if (feof (f))
	break;
      c = getc (f);
      if (c == '\r' || c == '\n')
	{
	  ungetc (c, f);
	  skipEOL (f);
	  continue;
	}
      if (c == ';' || c == '#')
	{
	  skipLine (f);
	  state = 0;
	  continue;
	}
      if (c == '"')
	{
	  if (state != 0)
	  {
	    fprintf (stderr, _("%s:%d Quote is not complete, %d field(s) missing\n"), filename, line - 1, QUOTE_FIELDS - state);
	    errors++;
	  }
	  state = 1;
	  delim = 0;
	  fprintf (i, "%-7ld\r\n", ftell (f) - 1);
	  count++;
	}
      else
	ungetc (c, f);
      if (state == 0 && c != '"')
	{
	  fprintf (stderr, _("%s:%d Line is not starting with \" character - ignoring.\n"), filename, line);
	  errors++;
	  skipLine (f);
	  continue;
	}
      while (1)
	{
	  c = getc (f);
	  if (c == EOF)
	    break;
	  if (c == '\r' || c == '\n')
	    {
	      ungetc (c, f);
	      skipEOL (f);
	      break;
	    }
	  if (state == 0)
	    {
	      fprintf (stderr, _("%s:%d Extra ending data on line\n"), filename, line);
	      skipLine (f);
	      break;
	    }

	  if (c == '\\')
	    {
	      if (escape == 0)
		escape = 1;
	      else
		escape = 0;
	      continue;
	    }
	  if (escape == 1)
	    {
	      if (c != 'n' && c != 'r' && c != 't' && c != '"')
		fprintf (stderr, _("%s:%d Unknown escape sequence \\%c\n"), filename, line, c);
	      escape = 0;
	      continue;
	    }
	  if (c == '"')
	    {
	      if (delim == 1)
		{
		  delim = 0;
		  continue;
		}
	      else
		{
		  if (state >= QUOTE_FIELDS)
		    state = 0;
		  delim = 1;
		  skipWhitespace (f);
		  continue;
		}
	    }
	  if (delim == 1)
	    {
	      if (c != ',')
	      {
		fprintf (stderr, _("%s:%d Bad field delimiter, expecting , got %c\n"), filename, line, c);
		errors++;
	      }
	      else
		state++;
	      skipWhitespace (f);
	      continue;
	    }
	}
    }

  fclose (i);
  fclose (f);
  chmod (tmpfn, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
  free (tmpfn);
  return count;
}
