#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#include <time.h>
#include "quote.h"


void
skipEOL (FILE * f)
{
  int c;
  char eol = 0;
  for (;;)
    {
      c = getc (f);
      if (c == EOF)
	return;
      if (c == '\n')
	return;
      if (c == '\r')
	{
	  eol = 1;
	  continue;
	}
      ungetc (c, f);
      return;
    }

}


void
skipWhitespace (FILE * f)
{
  int c;
  for (;;)
    {
      c = getc (f);
      if (c == EOF)
	return;
      if (c == ' ' || c == '\t')
	continue;
      ungetc (c, f);
      return;
    }

}

void
skipLine (FILE * f)
{
  int c;
  char eol = 0;
  for (;;)
    {
      c = getc (f);
      if (c == EOF)
	return;
      if (c == '\n')
	return;
      if (eol == 1)
	{
	  ungetc (c, f);
	  return;
	}
      if (c == '\r')
	{
	  eol = 1;
	  continue;
	}
    }
}

int
nextcharfromstring (char **str)
{
  char res;
  if (str == NULL)
    return EOF;
  if (*str == NULL)
    return EOF;
  if (**str == '\0')
    return EOF;

  res = **str;
  *str = *str + 1;

  if (res == '\t')
    return ' ';
  else if (res == '\\')
    {
      res = **str;
      *str = *str + 1;
      switch (res)
	{
	case '"':
	  return '"';
	case '\0':
	  *str = *str - 1;
	  return '\\';
	default:
	  return '\\';
	case 'n':
	  return '\n';
	case 't':
	  return '\t';
	case 'r':
	  return '\r';
	}

    }

  return res;
}

void 
backcharfromstring (char **str)
{
  if (str == NULL)
    return;
  if (*str == NULL)
    return;

  *str = *str - 1;
}

char *
wordfromstring (char **str)
{
  char *word = NULL;
  int c;
  if (str == NULL)
    return NULL;
  if (*str == NULL)
    return NULL;
  if (**str == '\0')
    return NULL;

  /* skip initial whitespace */
  while ((c = nextcharfromstring (str)) == ' ')
    ;
  while (1)
    {
      if (c == EOF)
	break;
      add_char_to_string (&word, c);
      c = nextcharfromstring (str);
      if (c <= ' ' && c>=0 )
      {
	      // printf("Stop=%c.\n",c);
	      break;
      }
    }
  // printf("Word: %s.\n",word);
  return word;
}

/* prida retezec word k retezci *line a provede realokaci */

char **
append_string(char **line,const char *word)
{
	if(*line==NULL)
	{
		*line=calloc(1,1);
	}
	if(*line==NULL) 
		return NULL;
	if (word==NULL)
		return line;
	*line=realloc(*line,strlen(*line)+strlen(word)+1);
	if(*line==NULL)
		return NULL;
	strcat(*line,word);
	return line;
}

/* vraci NULL if plna radka */
/* 8bit clean */
char **
add_word_to_line (char **line, const char *word, unsigned int maxsize)
{
  char new = 0;
  if (*line == NULL)
    {
      *line = calloc (1, 1);
      new = 1;
    }
  if (*line == NULL)
    return NULL;
  if (word == NULL)
    return line;
   // printf("Adding word: %s.\n",word);
  if (strlen (*line) + strlen (word) + 1 > maxsize)
    return NULL;

  *line = realloc (*line, strlen (*line) + strlen (word) + 2);
  if (*line == NULL)
    return NULL;
  if (new == 0)
    strcat (*line, " ");
  strcat (*line, word);

  return line;
}

char **
justify_line (char **line, int direction, unsigned int size, int wrap)
{
  int i;
  if (*line == NULL)
    return line;
  if (strlen (*line) >= size)
    return line;
  if (direction < JUSTIFY_MIN || direction > JUSTIFY_MAX)
    return NULL;
  *line = realloc (*line, size + 1);
  if (*line == NULL)
    return NULL;

  switch (direction)
    {
    case JUSTIFY_LEFT:
      if (wrap == 0)
	return line;
      while (strlen (*line) < size)
	strcat (*line, " ");
      return line;
    case JUSTIFY_RIGHT:
      i = size - strlen (*line);
      memmove (*line + i, *line, strlen (*line) + 1);
      for (--i; i >= 0; i--)
	*(*line + i) = ' ';
      return line;
    case JUSTIFY_CENTER:
      /* TODO not implemented! */
      return line;
    }
  return NULL;
}

void init_random()
{
#ifndef HAVE_SRANDOM
	FILE *rf;
	unsigned int seed;
	rf=fopen("/dev/urandom","rb");
	if(rf!=NULL) 
	{
		setvbuf(rf,NULL,_IONBF,0);
		if(fread(&seed,sizeof(seed),1,rf)>0)
		{
			fclose(rf);
			srand(seed);
			return;
		}
		fclose(rf);
	}
	srand (time (0) / getpid ());
#else
	srandomdev();
#endif
	return;
}
