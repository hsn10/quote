; cit�ty sezb�ran� z kn��ky
;
;  Milo� Zapletal: V�pravy za dobrodru�stv�m
; Vydal Albatros 1986
;
; Moto: Dobrodru�n� kn��ky jsou pln� p��hod, kter� pro�ili
;        jin�. Tato kniha je pln� dobrodru�stv�, kter� pro�ije� Ty.

"Pr� nejsou z�zraky. Ale v p��rod� m��ete kdekoli naj�t plno nejnepochopiteln�j��ch z�zrak�.",,"Univerzitn� profesor dr. J. Obenberger",""

"Opravdov� dobrodru�stv� nepro�ije ten, kdo sed� doma.",,"J. Joyce",""

"L�ska k p��rod� a sklon k toulk�m se rod� ve m�st�, kde je spousta lid�.
Z m�sta to t�hne �lov�ka do osam�losti a z pustin zase k lidem.",,"M. M. Pri�vin",""

"Nedovoluj sv�m d�tem zab�jet hmyz; t�m za��n� vra�d�n� lid�.",,"Pythagoras",""

"Netrhej kv�tiny, kdy� je chce� m�t. Jdi d�l a kv�ty budou kv�st u v�ech tv�ch cest.",,"Rab�ndran�th Th�kur",""

"Den z��i zatm�v� d�lky - noc tempem zapaluje v��e.",,"Otakar B�ezina",""

"Kdo se v ml�d� vyd�v� na cesty, podob� se kralevici ze star�ch poh�dek,
jde do nezn�ma, do daleka. Putuje osam�le, s�m nezn�m�, a hled� to, co jin�
se hledat neodv��.",,"M. Nevrl�",""

"Kdy� zabloud��, vzpome� si na to, co ��k�vali Indi�ni. Neztratil ses ty, ztratilo se tvoje t�p�.",,"E. Th. Seton",""

"Pt�ky jsi poznal a pu�ku nepou�il? Lekn�n jsi miloval, stonek mu neutrhl? �, bu� mi p��telem a u� m� b�t p��telem tob�.",,"H. D. Thoreau",""

"To nejkr�sn�j��, co m��eme za��t, je tajemstv�. Je to z�kladn� pocit, kter�
stoj� u kol�bky um�n� a v�dy. Kdo ho nezn� a kdo se u� neum� divit, kdo u�
neum� �asnout, ten je jako mrtv�.",,"A. Einstein",""

"Kdy� opou�t�� t�bo�i�t�, dokonale je ukli�. Nezne�i��uj krajinu, neni� jej�
kr�su.",,"Z u�en� indi�nsk�ho n��eln�ka Wabasa",""

"Pr�ce a z�bava, pravideln� se st��daj�c�, d�laj� �ivot radostn�m.",,"L. N. Tolstoj",""

"V p�se�n�m zrnku vid�t sv�t\ra nebe v lu�n� kv�tin�.\rDo dlan� vz�t nekone�no\ra v��nost pro��t v hodin�.",,"W. Blake",""

"Pohodl� �in� z lid� d�ti. Obt��e z nich d�laj� mu�e.",,"Smiles",""

"Jen m�t o�i k vid�n�\ruvid�t, co ka�d� z lid�\rnevid� a neuvid�.",,"J. �arek",""

"D�vno p�ed t�m, ne� �lov�k zapo�al svou ni�ivou �innost, pro kterou si
n�rokuje n�zev vl�dce p��rody, byl skute�n�m vl�dcem Zem� strom.",,"akademik B. N�mec",""

"Moje mysl t�m v�ce vzr�st�, ��m je moje okol� pust��. Dejte mi oce�n, pou��
nebo divo�inu.",,"H. D. Thoreau",""

"Celou p��rodu m�me na dosah, ale jen proto, aby se n�m otev�ely o�i, mus�me
ukojit svou touhu po d�lk�ch a pak se vr�tit dom�.",,"M. M. Pri�vin",""

"Vedla-li cesta za kulturou z lesa, pak cesta za lidstv�m vede nezbytn�
k lesu zp�t.",,"A. Hrdli�ka",""

"Ni�en� v�eho �iv�ho, pokud je to v prosp�chu �lov�ka, je jednou z nejv�znamn�j��ch lidsk�ch vlastnost�.",,"J. S. Proch�zka",""

"��m je �lov�k vzd�lan�j��, lep�� a u�lechtilej��, t�m v�ce si v�� p��rody.",,"J. A. Komensk�",""

"Pevn� v�le v�echno zm��e.",,"Vergilius",""

"Dokud jsme nehladov�li, nezn�me cenu j�dla.",,"Tatarsk� p��slov�",""

"Odnikud nen� tak p�kn� pohled na sv�t jako z vrcholku hory.",,"J. W. Goethe",""

"Nic nen� hor��ho, ne� kv�li de�ti, �nav� �i chladu nebo ze strachu p�ed hladem
a osam�lost� nedoj�t tam, kam si �lov�k p�ecevzal.",,"M. Nevrl�",""

"Ve v�d� obdiv a pocit tajemstv� stoj� na prahu nov�ch ob�v�.",,"akademik B. N�mec",""

"Um�n� inteligentn� naplnit voln� �as je posledn�m ovocem civilizace.",,"B. Russell",""

"Ka�d� rostlina, zv��e i �lov�k maj� v p��rod� vlastn� a osobit� domov,
bydli�t�, a jsou k n�mu pevn� v�z�ni nes�etn�mi neviditeln�mi pouty.",,"T. Farb",""

"Ch�ze je nejlep�� mo�n� cvi�en�. Zvykni si chodit velk� vzd�lenosti.",,"T. Jefferson",""

"Pamatuj, �e i kdy� jsi smrteln� �lov�k a m� omezen� �as k �ivotu, m��e�
dosp�t studiem p��rody k nekone�nu a v��nosti, uvid�t v�echno, co je,
co b�valo, a v�echno, co bude.",,"Epikuros",""
