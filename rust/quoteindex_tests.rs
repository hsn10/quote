//!
//!   Test suite for quoteindex module
//!
#![allow(non_snake_case)]

use super::*;
use rstest::*;

/* .X filename generator tests */

#[test]
fn indexFilenameNoExt() {
   assert_eq!(filename("lopata"),"lopata.X");
}

#[test]
fn indexFilenameReplaceExt() {
   assert_eq!(filename("karel.txt"),"karel.X");
}

#[test]
fn indexFilenameStartingDot() {
   assert_eq!(filename(".dotname"),".dotnameX");
}

/* index generator tests */

#[rstest]
fn twoLinesReindex() {
   let tf = testfile::from("\"one\"\r\n\"two\"");
   let rc = reindex(tf);
   assert! (&rc.is_ok());
   assert_eq! (rc.unwrap(),2);
}
