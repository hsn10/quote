//!
//! Operations on quote collection index
//!

/**
 * Collection index entry.
 *
 * Quote file name, number of quotes, quote file weight
*/
#[cfg_attr(test,derive(Debug, PartialEq))]
pub struct IndexEntry {
   /** quote file name */
   _0: String,
   /** quotes count */
   _1: usize,
   /** file weight */
   _2: f32
}

impl IndexEntry {
   /** Construct new IndexEntry
     * arguments: file name, quotes count, file weight
   */
   pub fn new(_0:String, _1:usize, _2:f32) -> Self {
      Self { _0, _1, _2 }
   }
}

impl Clone for IndexEntry {
   /** clone creates independant copy */
   fn clone (&self) -> Self {
      Self {
         _0 : self._0.clone(),
         _1 : self._1,
         _2 : self._2
      }
   }
}

/**
 Structure holding quote collection index
*/
pub struct CollectionIndex {
   /** quote file entries. Tupples of (filename, quotes count, file relative weight) */
   pub files: Vec<IndexEntry>,
   /** text after empty line */
   pub trailer: String,
   /** file location */
   pub path: std::path::PathBuf
}

/**
  Create new empty CollectionIndex
  # Returns
  empty CollectionIndex
*/
pub fn new() -> CollectionIndex {
   CollectionIndex { files:Vec::new(), trailer:String::new(), path: std::path::PathBuf::new() }
}

impl Clone for CollectionIndex {
   fn clone (&self) -> Self {
      Self { files:self.files.clone(), trailer:self.trailer.clone(), path:self.path.clone() }
   }
}

impl std::ops::Deref for CollectionIndex {
   type Target = Vec<IndexEntry>;

   fn deref(&self) -> &Self::Target {
      &self.files
   }
}

impl<'a> IntoIterator for &'a CollectionIndex {
   type Item = &'a IndexEntry;
   type IntoIter = std::slice::Iter<'a, IndexEntry>;

   fn into_iter(self) -> Self::IntoIter {
      self.files.iter()
   }
}

impl<'a> IntoIterator for &'a mut CollectionIndex {
   type Item = &'a mut IndexEntry;
   type IntoIter = std::slice::IterMut<'a, IndexEntry>;

   fn into_iter(self) -> Self::IntoIter {
      self.files.iter_mut()
   }
}

impl CollectionIndex {
   /**
   Writes collection index to file
   # Arguments
   * fname - index file name
   */
   pub fn write( &self, fname: impl AsRef<std::path::Path>) -> std::io::Result<()> {
      use std::io::Write;

      let f = std::fs::File::create (fname)?;
      let mut writer = std::io::BufWriter::new(f);

      for item in self {
         if item._2 != 1.0 {
            write!(writer, "{} {} {}\r\n", item._0, item._1, item._2)?;
         } else {
            write!(writer, "{} {}\r\n", item._0, item._1)?;
         }
      }
      if ! self.trailer.is_empty() {
         write!(writer, "\r\n{}", self.trailer)?;
      }
      writer.flush()?;
      Ok(())
   }

   /** Flushes collection into its original file */
   pub fn flush( &self ) -> std::io::Result<()> {
      self.write( &self.path )
   }

   /** Refreshes collection file */
   pub fn refresh( &mut self ) {
      // Step 1 - remove references to inaccessible files
      self.files.retain (|entry| {
         std::path::Path::new(&entry._0).exists()
      });
      // Step 2 - reindex
      for entry in self {
         println!("Counting records and indexing {}",entry._0);
         let rows = super::quoteindex::reindex(&entry._0).unwrap_or(0);
         *entry=entry.clone();
         entry._1 = rows;
         match rows {
            0 => println!(" ..  empty file."),
            _ => println!(" .. {} records found.", rows)
         }
      }
   }

   /** Returns the number of quotes in the collection. */
   pub fn len(&self) -> usize {
      self.files.iter().fold(0, |sum, i| sum + i._1 )
   }
}

/**
  Opens collection index file
  # Arguments
  * fname - index file name
*/
pub fn open( fname: impl AsRef<std::path::Path>) -> std::io::Result<CollectionIndex> {
   use std::io::Read;
   use std::io::BufRead;

   let f = std::fs::File::open (&fname)?;
   let mut reader = std::io::BufReader::new(f);
   let mut rc = new();
   rc.path.push(fname);
   let mut radka = String::new();
   while { radka.clear(); reader.read_line(&mut radka)? != 0 } {
      if radka.trim().is_empty() { break; }
      // print!("Line read: {}",radka);
      let mut split = radka.split_ascii_whitespace();
      rc.files.push( IndexEntry::new(
         match split.next() {
            Some(x) => { x.to_string() },
            None => { continue }
         },
         match split.next() {
            Some(x) => {
               match x.to_string().parse::<usize>() {
                  Ok(r) => { r },
                  Err(_) => { 0 }
               }
            },
            None => { 0 }
         },
         match split.next() {
            Some(x) => {
               match x.to_string().parse::<f32>() {
                  Ok(r) => { r },
                  Err(_) => { 1.0 }
               }
            },
            None => { 1.0 }
         }
      ) );
   }
   let mut buffer = Vec::new();
   // read rest of file
   reader.read_to_end(&mut buffer)?;
   rc.trailer.push_str(std::string::String::from_utf8_lossy(&buffer).trim_end_matches('\x1A'));

   Ok(rc)
}

#[cfg(test)]
#[path= "./collectionindex_tests.rs"]
mod tests;

#[cfg(test)]
#[path="."]
mod indexentry {
   #[path= "./collectionindex_indexentry_tests.rs"]
   mod tests;
}
