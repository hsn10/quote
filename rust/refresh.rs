/* Based on REXX Refresh 0.12
      added ability to reindex more than one index */

#![forbid(unsafe_code)]

use std::env;

mod collectionindex;
mod quoteindex;

use std::process::ExitCode;

/**
 * Refresh command line utility entry point.
 * Usage: refresh [index] ...
 *
 * Refreshes quote index which containing 
 *    name of quote file with number of quotes and CR LF.
 *
 * First empty line stop processing of index.
 * References to non existing files are deleted from index.
 *
 * Exit Errorlevels:
 *   0 - OK
 *   1 - Index file cannot be opened
 *   2 - Error creating temporary file
 *   3 - Some index files could not be opened, but at least one index
 *         got succesfully processed (added in rust version)
*/
#[allow(non_snake_case)]
#[allow(unused_parens)]
fn main() -> ExitCode {
 
 /** command line arguments without program name */
 fn arg() -> impl std::iter::Iterator<Item = String> {
    env::args().skip(1)
 }

 /** what we are going to index */
 fn idx() -> Box<dyn std::iter::Iterator<Item = String>> {
    if arg().count() == 0 { Box::new(std::iter::once(String::from("index"))) } else { Box::new(arg()) }
 }

 println!("[DEBUG] refresh invoked with {} args.", idx().count());

 let mut someIndexed = false;
 let mut someNotFound = false;
 let mut writeError = false;

 idx().for_each(|ixf| {
    println!("Refreshing quote index file {}", ixf);
    let index = collectionindex::open(ixf); // returns Result <index>
    index.map_or_else( |_| someNotFound = true,
       |mut i| { 
          i.refresh();
          i.flush().map_or_else( |_| writeError = true, |_| someIndexed = true );
    } );
    
    //let _idontcare = index.map(|mut i| {i.refresh();i}).map(|i| i.flush());
 });
 println!();
 println!("Refresh was succesfull.");
 match ( someIndexed ) {
    true => match ( someNotFound ) {
       true  => ExitCode::from(3),
       false => ExitCode::SUCCESS
    },
    false => match ( writeError ) {
       true  => ExitCode::from(2),
       false => ExitCode::from(1)
    }
 }
}
