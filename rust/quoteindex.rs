//!
//! Operations on quote index .X file
//!

/**
   Reindex a quote file. Writes down a .X index file.
   Returns number of indexed quotes.
*/
pub fn reindex<T: AsRef <std::path::Path>>(file: T) -> std::io::Result<usize> {
   use std::fs::OpenOptions;
   use std::io::{Seek,SeekFrom};

   let     fq  = OpenOptions::new().read(true).write(false).create(false).open(&file)?;
   let     fx  = OpenOptions::new().read(false).write(true).truncate(true).create(true).open(filename(&file))?;
   let mut jm = std::io::BufReader::new(fq);
   let mut ix = std::io::BufWriter::new(fx);

   /* check quote file size */
   let n = jm.seek(SeekFrom::End(0))?;
   /* is file too short to have meaningful content? */
   if n < 5 { return Ok(0) }
   /* seek back to start */
   jm.rewind()?;
   let mut sp = 0u64;

   let mut linek = 0;
   let mut linka = String::new();
   while sp < n {
       linka.clear();
       <dyn std::io::BufRead>::read_line(&mut jm, &mut linka)?;
       linka = String::from(linka.trim());
       if linka.starts_with('\"') {
           <dyn std::io::Write>::write(&mut ix,format!("{:>8}\r\n",sp).as_bytes())?;
           linek += 1;
       }
       sp = jm.stream_position()?;
   }
   Ok(linek)
}

/**
   Returns name of index file derived from base quote file name.

   Quote index has .X extension. Extension is added or replaced unless
   quote file starts with dot - in that case X is appended.
*/
pub fn filename(basefile: impl AsRef<std::path::Path>) -> String {

   /* convert basefile to String */
   let base: String = basefile.as_ref().to_str().unwrap().to_string();
   if base.starts_with('.') {
      let mut rc = base; rc.push('X'); rc
   } else {
   match base.rfind('.')  {
      None =>    { let mut rc = base; rc.push_str(".X"); rc }
      Some(x) => { let (first, _) = base.split_at(x+1); let mut rc = String::from(first);
                   rc.push('X'); rc }
   }
   }
}

#[cfg(test)]
#[path= "./quoteindex_tests.rs"]
mod tests;
