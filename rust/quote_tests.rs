//!
//!   Test suite for quote executable
//!
#![allow(non_snake_case)]
#![allow(unused_imports)]

use super::*;
use rstest::*;

/**
 Check if random numbers are distinct enough
*/
#[test]
fn nextLong_distinct_test() {
   const TESTSIZE:usize = 5000;
   const MAXDUPES:usize = 2;

   let mut dupecount:usize = 0;
   let mut generatedNumbers = std::collections::HashSet::<u64>::new();
   generatedNumbers.reserve(TESTSIZE);

   for _ in 1..=TESTSIZE {
      let rnd = nextLong();
      if ( generatedNumbers.contains(&rnd) ) { dupecount += 1; }
      generatedNumbers.insert(rnd);
   }

   assert! ( dupecount <= MAXDUPES );
}

/**
 Check if random number bounds are checked
*/
#[test]
#[should_panic]
fn random_invalid_bounds_checked() {
   random(20,5);
}

/**
 Check same random number bounds.
 Should not panic
*/
#[test]
fn random_same_bounds() {
   assert_eq!(random(5,5), 5);
   assert_eq!(random(0,0), 0);
}

/**
 Check if requested bounds are respected.
*/
#[test]
fn random_within_bounds() {
   const TESTSIZE:usize = 5000;
   const LOW:u64 = 10;
   const HIGH:u64 = 30;

   for _ in 1..=TESTSIZE {
      let r = random(LOW,HIGH);
      assert! ( r>= LOW );
      assert! ( r<= HIGH );
   }
}

/**
 Check if requested bounds limits are hit.
*/
#[test]
fn random_boundaries_hit() {
   const TESTSIZE:usize = 2000;
   const LOW:u64 = 10;
   const HIGH:u64 = 20;

   let mut lowhit = false;
   let mut hihit  = false;

   for _ in 1..=TESTSIZE {
      let r = random(LOW,HIGH);
      if ( r==LOW  ) { lowhit = true; }
      if ( r==HIGH ) { hihit = true; }
   }
   assert! ( lowhit, "low boundary not hit" );
   assert! ( hihit, "high boundary not hit" );
}
