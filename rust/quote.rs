/* Based on Quote REXX Engine 0.09 */

#![forbid(unsafe_code)]
#![allow(unused_parens)]
#![allow(non_snake_case)]

/** 
 * errorlevels: 
 * 0 - successfull termination
 * 1 - index file is unreadable
 * 2 - no quotes found in index file
*/

mod collectionindex;
mod quoteindex;

use std::process::ExitCode;

/** returns random u64 number */
fn nextLong() -> u64 {
   std::hash::Hasher::finish(
      &std::hash::BuildHasher::build_hasher(
         &std::collections::hash_map::RandomState::new()
   ))
}

/** returns random number.
    min and max parameters are inclusive.
*/
fn random(min:u64, max:u64) -> u64 {
   if (max < min) {
      panic!("minimum number must be lower or equal to maximum");
   } else if ( max == min ) {
     min
   } else {
     min + ( nextLong() % ( max - min + 1) )
   }
}

fn main() -> ExitCode {

   /* get index file name from command line arguments,
      fallback to "index".
    */
   let i =
   {
      let args: Vec<String> = std::env::args().collect();
      if ( args.len() > 1 )
      {
         String::from(&args[1])
      }  else
      {
         String::from("index")
      }
   };
   let ch = collectionindex::open(i);
   if (ch.is_ok()) {
      let idx = ch.unwrap();
      let citatu = idx.len();
      if ( citatu == 0 ) {
         // no citations found in index
         ExitCode::from(2)
      } else {
         println!("{} quotes found.", citatu);

         ExitCode::SUCCESS
      }
   } else {
      ExitCode::from(1)
   }
}

#[cfg(test)]
#[path= "./quote_tests.rs"]
mod quotetests;
