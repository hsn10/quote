//!
//!   Test suite for collectionindex module
//!
#![allow(non_snake_case)]

use super::*;

/**
Test if new() method returns empty CollectionIndex.
*/
#[test]
fn newReturnsEmpty() {
   let tv = new();
   assert_eq!(tv.files.iter().count(),0);
   assert_eq!(tv.trailer.is_empty(),true);
}

/**
Load index in Basic format.
No trailer, no weights, two lines.
*/
#[test]
fn openBasic() {
   let tf = testfile::from("file.ext 3\r\nsecond.txt 5\r\n");
   let ci = open(tf);
   assert!(ci.is_ok());
   let ci = ci.unwrap();
   assert_eq!(ci.files.iter().count(),2);
   // println!("Files read:\n{:#?}",ci.files);
   let row = ci.files.get(0).unwrap();
   assert_eq!(row._0,"file.ext");
   assert_eq!(row._1,3);
   assert_eq!(row._2,1.0);
   let row = ci.files.get(1).unwrap();
   assert_eq!(row._0,"second.txt");
   assert_eq!(row._1,5);
   assert_eq!(row._2,1.0);
}

/**
Load index in Extended format.
No trailer, weights, two lines.
*/
#[test]
fn openWithWeights() {
   let tf = testfile::from("file.ext 3 1.2\r\nsecond.txt 5 1.6\r\n");
   let ci = open(tf);
   assert!(ci.is_ok());
   let ci = ci.unwrap();
   assert_eq!(ci.files.iter().count(),2);
   // println!("Files read:\n{:#?}",ci.files);
   let row = ci.files.get(0).unwrap();
   assert_eq!(row._0,"file.ext");
   assert_eq!(row._1,3);
   assert_eq!(row._2,1.2);
   let row = ci.files.get(1).unwrap();
   assert_eq!(row._0,"second.txt");
   assert_eq!(row._1,5);
   assert_eq!(row._2,1.6);
}

/**
Load index with trailer.
One line.
*/
#[test]
fn openWithTrailer() {
   let tf = testfile::from("trailer.txt 2\r\n\r\ntrailer here\r\n");
   let ci = open(tf);
   assert!(ci.is_ok());
   let ci = ci.unwrap();
   assert_eq!(ci.files.iter().count(),1);
   // println!("Files read:\n{:#?}",ci.files);
   let row = ci.files.get(0).unwrap();
   assert_eq!(row._0,"trailer.txt");
   assert_eq!(row._1,2);
   assert_eq!(row._2,1.0);
   // trailer
   assert_eq!(ci.trailer,"trailer here\r\n");
}

/**
Load index with trailer.
one line and finished with 1A.
We should not load terminating 1A into trailer.
*/
#[test]
fn openWithTrailer1A() {
   let tf = testfile::from("trailer.txt 2\r\n\r\ntrailer here\r\n\x1A");
   let ci = open(tf);
   assert!(ci.is_ok());
   let ci = ci.unwrap();
   assert_eq!(ci.files.iter().count(),1);
   let row = ci.files.get(0).unwrap();
   assert_eq!(row._0,"trailer.txt");
   assert_eq!(row._1,2);
   assert_eq!(row._2,1.0);
   // trailer
   assert_eq!(ci.trailer,"trailer here\r\n");
}

/**
Load index without quote count.
One line.
*/
#[test]
fn openWithoutCount() {
   let tf = testfile::from("trailer.txt\r\n");
   let ci = open(tf);
   assert!(ci.is_ok());
   let ci = ci.unwrap();
   assert_eq!(ci.files.iter().count(),1);
   let row = ci.files.get(0).unwrap();
   assert_eq!(row._0,"trailer.txt");
   assert_eq!(row._1,0);
   assert_eq!(row._2,1.0);
}

/**
Write Index loaded without counts.
*/
#[test]
fn writeLoadWithoutCount() {
   let tf = testfile::from("trailer.txt\r\n");
   let c1 = open(tf);
   assert!(c1.is_ok());
   let c1 = c1.unwrap();
   assert_eq!(c1.files.iter().count(),1);
   let row = c1.files.get(0).unwrap();
   assert_eq!(row._0,"trailer.txt");
   assert_eq!(row._1,0);
   assert_eq!(row._2,1.0);

   let n = testfile::generate_name();
   assert!(c1.write(&n).is_ok());

   let c2 = open(n);
   assert!(c2.is_ok());
   let ci = c2.unwrap();
   assert_eq!(ci.files.iter().count(),1);
   let row = ci.files.get(0).unwrap();
   assert_eq!(row._0,"trailer.txt");
   assert_eq!(row._1,0);
   assert_eq!(row._2,1.0);
}

#[test]
fn iterateOverDirectIntoIter() {
   let mut tv = new();
   let item : IndexEntry = IndexEntry::new("a".to_string(),1,1.0);
   tv.files.push( item.clone() );
   let fetch = tv.into_iter().next().unwrap();
   assert_eq!(fetch, &item);
}

#[test]
fn iterateOverForLoop() {
   let mut tv = new();
   let item : IndexEntry = IndexEntry::new("a".to_string(),1,1.0);
   tv.files.push( item.clone() );
   let mut ok = false;
   for v in &tv {
      assert_eq!(v, &item);
      ok = true;
   }
   assert_eq! ( ok, true );
}

/**
Count number of quotes
*/
#[test]
fn checkComputedLen() {
   let tf = testfile::from("file.ext 3\r\nsecond.txt 5\r\n");
   let ci = open(tf);
   assert!(ci.is_ok());
   assert_eq!( ci.unwrap().len(), 8);
}

/**
Check if cloned Collection Index is not liked to original.
*/
#[test]
fn checkCloneIsIdependant() {
   let tf = testfile::from("file.ext 3\r\nsecond.txt 5\r\n");
   let c1 = open(tf);
   assert!(c1.is_ok());
   let mut c1 = c1.unwrap();

   let c2 = c1.clone();

   assert_eq!( c1.len(), 8);
   assert_eq!( c2.len(), 8);

   c1.files.clear();

   assert_eq!( c1.len(), 0);
   assert_eq!( c2.len(), 8);
}