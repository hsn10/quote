//!
//!   Test suite for collectionindex / IndexEntry structure
//!
#![allow(non_snake_case)]

use super::super::*;

/**
Check if IndexEntry new() method fills in fields.
*/
#[test]
fn newFillsFields() {
   let _0 = "file.ext".to_string();
   let _1 = 10;
   let _2 = 1.1;

   assert_eq!( _0, _0.clone());
   let tv = IndexEntry::new(_0.clone(), _1, _2);

   assert_eq!( _0, tv._0);
   assert_eq!( _1, tv._1);
   assert_eq!( _2, tv._2);
}

/**
Check if cloned IndexEntry is not liked to original.
*/
#[test]
fn CloneIsIdependant() {
   let _0 = "file.ext".to_string();
   let _1 = 10;
   let _2 = 1.1;

   let i1 = IndexEntry::new(_0.clone(), _1, _2);
   let mut i2 = i1.clone();

   // clones are equal
   assert_eq!( i1._0, i2._0);
   assert_eq!( i1._1, i2._1);
   assert_eq!( i1._2, i2._2);

   // string is non zero sized
   assert! (i1._0.len() > 0 );
   assert! (i2._0.len() > 0 );

   // clear sting and check if other clone is unmodified
   i2._0.clear();
   assert_eq!( i2._0.len(), 0 );
   assert!   ( i1._0.len() > 0 );
}