# File formats

### Collection Index file format
*  usual extension *.i*
*  holds file names and number of quotes inside each file
*  Basic file encoding is ASCII
*  It can also be UTF-8 if BOM is found to support non ASCII characters
   in file name
*  line ending is CR LF
*  first empty line ends index processing
*  there might be a trailing text after empty line which is ignored
*  file may have 1A character at the end
*  initialy created by hand
*  Number of quotes inside updated by refresh tool

filename \<SP> pocet_citatu(int) [ \<SP> vaha_souboru(float) ] CR LF

### Quote index file format
* extension is *.X*
* strict ASCII file encoding
* it is created and rebuild by refresh utility
* consists of lines pointing to quotes in quote file for faster access
* left space padded to length 8 byte offset in ascii representation CR LF
* each record is 10 bytes long: 8 bytes of ascii encoded position and 2 bytes of
  CRLF
* Quote must start bellow first 100MB of file
* No trailing text or 1A character at end allowed

## Configuration formats

### Group configuration file
-----------------------------

```
# comment until new line
group gita [opt. Group priorify]
 options dupes|nodupes
 index /usr/share/gita.i [weight]
 file /usr/gita.txt [weight]
 fortune /usr/share/fortunes/ [weight]
           [dir or file]
 directory /usr/ [weight]
endgroup
```

### user configuration file

```
event login
priority gita 1.2
enable [group] ...  -> nastavi priority u group na 1.0, pokud je nulova
disable [group] ... -> nastavi priority na 0.0

disable *           -> zakaze vse
endevent

event *             -> default event
```

### Unicode BOM ###

Used to detect file format.

|Byte order mark|Encoding |
|----------|--------------|
| EF BB BF |  UTF8        |
| FE FF    |  UTF16 BE    |
| FF FE    |  UTF16 LE    |
